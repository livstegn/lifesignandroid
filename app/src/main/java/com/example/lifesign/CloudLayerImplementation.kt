package com.example.lifesign

import java.net.URL


class CloudLayerImplementation : CloudLayer {

    override fun getPostnumreCloud () : String {
        val url = "https://dawa.aws.dk/postnumre?nr=8600"

        val result = URL(url).readText()

        return result
    }

    override fun getUsers () : String {
        val url = "https://y15h3ayew7.execute-api.eu-central-1.amazonaws.com/test/users"

        val result = URL(url).readText()

        return result
    }

}