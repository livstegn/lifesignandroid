package com.example.lifesign

import org.json.JSONObject
import java.util.*

class User {
    var userId = String()
    var userName = String()
    var email = String()
    var cellno = String()
    var status = UserStatusType.creating
    var deviceId = String()
    var platform = PlatformType.android

    public constructor(jsonObject : JSONObject) {
        this.userId = jsonObject.optString("partitionkey")
        this.userName = jsonObject.optString("name")
        this.email = jsonObject.optString("data-sk")
        this.status = UserStatusType.valueOf(jsonObject.optString("status"))
        this.deviceId = jsonObject.optString("deviceid")
    }
}

enum class UserStatusType {
    invited, creating, created, active, closing
}

enum class PlatformType {
    iOS, android, browser
}