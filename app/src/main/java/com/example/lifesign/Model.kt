package com.example.lifesign

import org.json.JSONArray
import org.json.JSONTokener

class Model {

    private lateinit var _cloudLayer : CloudLayer


    public constructor (cl: CloudLayer) {
        _cloudLayer = cl
    }

    fun getPostnumre () : String {
        val x = _cloudLayer.getPostnumreCloud()

        return x
    }

    fun getUsers () : ArrayList<User> {
        var users : ArrayList<User> = ArrayList<User>(0)
        val jsonArrayString = _cloudLayer.getUsers()
        val jsonArray = JSONTokener(jsonArrayString).nextValue() as JSONArray

        for (i in 0 until jsonArray.length()) {
            val user = User(jsonArray.getJSONObject(i))

            users.add(user)
        }

        return users
    }
}